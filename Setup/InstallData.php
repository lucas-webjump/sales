<?php

namespace Webjump\Sales\Setup;

use Magento\Sales\Setup\SalesSetupFactory;
use Webjump\Sales\Api\StatusSAPInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\SalesSequence\Model\Builder;
use Magento\SalesSequence\Model\Config as SequenceConfig;


/**
 * Class InstallData
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Sales setup factory
     *
     * @var SalesSetupFactory
     */
    private $salesSetupFactory;

    /**
     * @var Builder
     */
    private $sequenceBuilder;

    /**
     * @var SequenceConfig
     */
    private $sequenceConfig;

    /**
     * @param SalesSetupFactory $salesSetupFactory
     * @param Builder $sequenceBuilder
     * @param SequenceConfig $sequenceConfig
     */
    public function __construct(
        SalesSetupFactory $salesSetupFactory,
        Builder $sequenceBuilder,
        SequenceConfig $sequenceConfig
    ) {
        $this->salesSetupFactory = $salesSetupFactory;
        $this->sequenceBuilder = $sequenceBuilder;
        $this->sequenceConfig = $sequenceConfig;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();
        /** @var \Magento\Sales\Setup\SalesSetup $salesSetup */
        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);

        /**
         * Install eav entity types to the eav/entity_type table
         */
        $salesSetup->installEntities();

        /**
         * Install order statuses from config
         */

        // add 'NEW_ATTRIBUTE' attributes for order
        $data = [];
        $statuses = [
            StatusSAPInterface::SALES_ORDER_CREATED => __(StatusSAPInterface::SALES_ORDER_CREATED_LABEL),
            StatusSAPInterface::REQUEST_PREPARATION => __(StatusSAPInterface::REQUEST_PREPARATION_LABEL),
            StatusSAPInterface::INVOICED_ORDER => __(StatusSAPInterface::INVOICED_ORDER_LABEL),
            StatusSAPInterface::ORDER_ISSUED => __(StatusSAPInterface::ORDER_ISSUED_LABEL),
            StatusSAPInterface::ORDER_DELIVERED_CLOSED => __(StatusSAPInterface::ORDER_DELIVERED_CLOSED_LABEL),
            StatusSAPInterface::DELIVERY_ORDER_RETURNED => __(StatusSAPInterface::DELIVERY_ORDER_RETURNED_LABEL)
        ];

        foreach ($statuses as $code => $info) {
            $data[] = ['status' => $code, 'label' => $info];
        }
        $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);

        /**
         * Install order states from config
         */
        $data = [];
        $states_one = [
            'processing' => [
                'label' => __(StatusSAPInterface::SALES_ORDER_CREATED_LABEL),
                'statuses' => [StatusSAPInterface::SALES_ORDER_CREATED => ['default' => '1']],
                'visible_on_front' => true,
            ],
            'complete' => [
                'label' => __(StatusSAPInterface::ORDER_ISSUED_LABEL),
                'statuses' => [StatusSAPInterface::ORDER_ISSUED => ['default' => '1']],
                'visible_on_front' => true,
            ],
        ];

        foreach ($states_one as $code => $info) {
            if (isset($info['statuses'])) {
                foreach ($info['statuses'] as $status => $statusInfo) {
                    $data[] = [
                        'status' => $status,
                        'state' => $code,
                        'is_default' => is_array($statusInfo) && isset($statusInfo['default']) ? 1 : 0,
                    ];
                }
            }
        }

        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default'],
            $data
        );

        $data = [];
        $states_two = [
            'processing' => [
                'label' => __(StatusSAPInterface::REQUEST_PREPARATION_LABEL),
                'statuses' => [StatusSAPInterface::REQUEST_PREPARATION => ['default' => '1']],
                'visible_on_front' => true,
            ],
            'complete' => [
                'label' => __(StatusSAPInterface::ORDER_DELIVERED_CLOSED_LABEL),
                'statuses' => [StatusSAPInterface::ORDER_DELIVERED_CLOSED => ['default' => '1']],
                'visible_on_front' => true,
            ],
        ];

        foreach ($states_two as $code => $info) {
            if (isset($info['statuses'])) {
                foreach ($info['statuses'] as $status => $statusInfo) {
                    $data[] = [
                        'status' => $status,
                        'state' => $code,
                        'is_default' => is_array($statusInfo) && isset($statusInfo['default']) ? 1 : 0,
                    ];
                }
            }
        }

        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default'],
            $data
        );

        $data = [];
        $states_tree = [
            'processing' => [
                'label' => __(StatusSAPInterface::INVOICED_ORDER_LABEL),
                'statuses' => [StatusSAPInterface::INVOICED_ORDER => ['default' => '1']],
                'visible_on_front' => true,
            ],
            'complete' => [
                'label' => __(StatusSAPInterface::DELIVERY_ORDER_RETURNED_LABEL),
                'statuses' => [StatusSAPInterface::DELIVERY_ORDER_RETURNED => ['default' => '1']],
                'visible_on_front' => true,
            ],
        ];

        foreach ($states_tree as $code => $info) {
            if (isset($info['statuses'])) {
                foreach ($info['statuses'] as $status => $statusInfo) {
                    $data[] = [
                        'status' => $status,
                        'state' => $code,
                        'is_default' => is_array($statusInfo) && isset($statusInfo['default']) ? 1 : 0,
                    ];
                }
            }
        }

        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default'],
            $data
        );

        /** Update visibility for states */
        $states = ['processing', 'complete', 'processing', 'complete', 'processing', 'complete'];
        foreach ($states as $state) {
            $setup->getConnection()->update(
                $setup->getTable('sales_order_status_state'),
                ['visible_on_front' => 1],
                ['state = ?' => $state]
            );
        }
        $setup->endSetup();
    }
}
