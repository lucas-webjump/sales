<?php

namespace Webjump\Sales\Api;

interface StatusSAPInterface
{
	/**
	 * Status of order in processing
	 */
	const SALES_ORDER_CREATED = 'ordem_venda_criada';
	const REQUEST_PREPARATION = 'pedido_preparacao';
	const INVOICED_ORDER = 'pedido_faturado';

	const SALES_ORDER_CREATED_LABEL = 'Ordem de Venda Criado';
	const REQUEST_PREPARATION_LABEL = 'Pedido em Preparação';
	const INVOICED_ORDER_LABEL = 'Pedido Faturado';

	/**
	 * Status of order in complete
	 */
	const ORDER_ISSUED = 'pedido_expedido';
	const ORDER_DELIVERED_CLOSED = 'pedido_delivered_closed';
	const DELIVERY_ORDER_RETURNED = 'pedido_entrega_devolvida';

	const ORDER_ISSUED_LABEL = 'Pedido expedido';
	const ORDER_DELIVERED_CLOSED_LABEL = 'Pedido entregue/fechado';
	const DELIVERY_ORDER_RETURNED_LABEL = 'Pedido entregue devolvido';

}